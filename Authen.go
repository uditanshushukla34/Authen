/* Created by - Uditanshu Shukla
This is a Authentication Utility for AK (JWT) validation
Input - Auth_Key(AK), GLUSR_ID, CLIENT_IP. 
Output - map[string]interface and Bool 
map[string]interface - Direct value with Json Response
Bool - boolean value indicating validated Token or not.
*/


package Authen

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"
)

var sharedKey = []byte("mysecretKey@uditanshu") //secret Key


func AKValidate(auth_Key ,glusr_id ,client_ip string) (map[string]interface{},bool) {
	validated := false
	var leeway int64 = 60

	if auth_Key != "" {
		token_string := strings.Split(auth_Key,".")
		if len(token_string) != 3{
			return map[string]interface{}{
				"CODE":"403",
				"MESSAGE":"Authentication Failed",
				"STATUS":"FAILURE",
				"APP_AUTH_FAILURE_CODE":"403",
			},validated 
		}

		header := token_string[0]
		payload := token_string[1]
		signnature := token_string[2]

		decodedHeader , err := Base64Decode([]byte(header))
		if err!=nil{
			return map[string]interface{}{
				"CODE":"400",
				"MESSAGE":"Error while decoding token!",
			},validated 
		}
		
		decodedPayload, err := Base64Decode([]byte(payload))
		if err!=nil{
			return map[string]interface{}{
				"CODE":"400",
				"MESSAGE":"Error while decoding token!",
			},validated
		}

		decodedSignature , err := Base64Decode([]byte(signnature))
		if err!=nil{
			return map[string]interface{}{
				"CODE":"400",
				"MESSAGE":"Error while decoding token!",
			},validated
		}

		data := header + "." + payload
		tokenHeader := make(map[string]interface{})
		tokenPayload := make(map[string]interface{})

		err = json.Unmarshal([]byte(decodedHeader), &tokenHeader)
		if err!=nil{
			return map[string]interface{}{
				"CODE":"404",
				"MESSAGE":"Error while parsing the token!",
			},validated
		}
		
		err = json.Unmarshal([]byte(decodedPayload), &tokenPayload)
		if err!=nil{
			return map[string]interface{}{
				"CODE":"404",
				"MESSAGE":"Error while parsing the token!",
			},validated
		}

		TokenExpiryTime := int64(tokenPayload["exp"].(float64))
		TokenIatTime := int64((tokenPayload["iat"]).(float64))
		TokenIssValue := tokenPayload["iss"].(string)
		currentTime := time.Now().Unix() //unix time stamp i have 26-08-2022 10-50

		if ( currentTime - leeway ) >= TokenExpiryTime{ //expiry time should be more 
			return map[string]interface{}{
				"CODE": "402",
				"MESSAGE" : "Token Expired",
				"STATUS":"FAILURE",
				"APP_AUTH_FAILURE_CODE":"402",
			},validated
		}

		message1 := []byte(data)
		hash_form := hmac.New(sha256.New,sharedKey)
		hash_form.Write(message1)
		generatedSignature := hash_form.Sum(nil)
		validated := hmac.Equal(generatedSignature, []byte(decodedSignature))

		if !validated{  //if false
			return map[string]interface{}{
				"CODE":"401",
				"MESSAGE":"Authentication Failed",
				"STATUS":"FAILURE",
				"APP_AUTH_FAILURE_CODE":"401",
			},validated
		}

		if TokenIatTime > ( currentTime + leeway ){ // not issue time > curren time i.e, issue time should be more than current
			validated = false
			return map[string]interface{}{
				"CODE":"404",
				"MESSAGE":"Cannot handle token prior to "+fmt.Sprint(time.Unix(TokenIatTime,0)),
				"STATUS":"FAILURE",
				"APP_AUTH_FAILURE_CODE":"404",
			},validated
		}

		if TokenIssValue == "USER"{
			if glusr_id != tokenPayload["sub"]{
				return map[string]interface{}{
					"CODE":"402",
					"MESSAGE":"GLID mismatch",
					"STATUS":"FAILURE",
					"APP_AUTH_FAILURE_CODE":"402",
				},validated
			}else{
				validated = true
				return map[string]interface{}{
					"CODE":"200",
					"MESSAGE":"Valid Authentication",
					"STATUS":"SUCCESS",
				},validated
			}
		}


		if TokenIssValue == "EMPLOYEE"{
			if tokenPayload["sub"] == ""{
				return map[string]interface{}{
					"CODE":"404",
					"MESSAGE":"JWT VALIDATED BUT EMPLOYEE_ID of PAYLOAD is EMPTY in EMPLOYEE TOKEN",
				},validated
			}else{
				return map[string]interface{}{
					"CODE":"200",
					"MESSAGE":"Valid Authentication",
				},validated
			}
		}

		boolIp := false
			if TokenIssValue == "CRON" {
				if tokenPayload["aud"] != ""{
					reqIp:=strings.Split(client_ip,":")[0]
					boolIp, _ = regexp.MatchString(reqIp,tokenPayload["aud"].(string))
			} else {
				boolIp = false
			}
			if boolIp {
				return map[string]interface{}{
					 "CODE":"200",
					 "MESSAGE":"Valid Glusr Token",
					},boolIp
			} else {
				if tokenPayload["aud"] != ""{
					return map[string]interface{}{
						"CODE":"404",
						"MESSAGE":"JWT VALIDATED BUT INPUT IP DIDNOT MATCH WITH PAYLOAD IP IN CRON TOKEN",
					},boolIp
				}else{
					return map[string]interface{}{
						"CODE":"404",
						"MESSAGE":"JWT VALIDATED BUT aud key is EMPTY in CRON TOKEN",
					},boolIp
				}	
			}				
		}

	}
	return map[string]interface{}{
		"CODE":"504",
		"MESSAGE":"Invalid AK Key",
	},validated
}

var pad = []byte("=")
func Base64Decode(src []byte) ([]byte, error) {
	if n := len(src) % 4; n > 0 {
		src = append(src, bytes.Repeat(pad, 4-n)...)
	}
	buf := make([]byte, base64.URLEncoding.DecodedLen(len(src)))
	n, err := base64.URLEncoding.Decode(buf, src)
	return buf[:n], err
}